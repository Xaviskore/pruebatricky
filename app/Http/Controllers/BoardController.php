<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\models\Players;
use App\models\Board;

class BoardController extends Controller
{
    //varaible para generar id unico de partida
    public $uniqueId;

    public function __Construct(){
        $this->uniqueId = uniqid();
    }

    //Pantalla de juego del programa
    public function index(){
        $result = $this->getPlayerTurnNS();
        $board = $this->getBoardData();
        if(!$board){
            return redirect()->route('home');
        }else{
            return view('game',compact('result','board'));
        }
    }

    //Pantalla del resultado de lapartida
    public function finish(){
        $board = $this->getBoardData();
        $players = $this->getPlayersData();
        if($board){
            return view('finish',compact('board','players'));
        }else{
            return redirect()->route('home');
        }
    }

    //Pantalla de generacion de nuevos jugadores e inicio de partida
    public function new(){
        $players = Players::get()->first();
        $board = $this->getBoardData();
        if($board){
            return redirect()->route('join');
        }else{
            return view('new', compact('players'));
        }
    }

    public function joinPlay(Request $request){
        return redirect()->route('home');
    }

    //Función para guardar los jugadores e inicializar la partida
    public function savePlayers(Request $request){
        //$player = Players::where('id','1')->get();
        $player = Players::find(1);
        $player->game_Id = $this->uniqueId;
        $player->playerOneName = $request->input('player-x');
        $player->save();
        if($this->initBoard()){
            $page = "board.index";
        }else{
            $page = "home";
        }
        return redirect()->route($page);
    }

    //Inicializar tablero del juego
    public function initBoard(){
        $board = new Board();
        $board->id = 1;
        $board->gameId = $this->uniqueId;
        $board->pos1 = 0;
        $board->pos2 = 0;
        $board->pos3 = 0;
        $board->pos4 = 0;
        $board->pos5 = 0;
        $board->pos6 = 0;
        $board->pos7 = 0;
        $board->pos8 = 0;
        $board->pos9 = 0;
        $board->totalMoves = 0;
        if($board->save()){
            $return = true;
        }else{
            $return = false;
        }
        return $return;
    }

    //Obtener el nombre del jugador del turno
    public function getPlayerTurnNS(){
        //$player = Players::where('gameId','')->get();
        $result = array();
        $player = Players::find(1);
        if($player->playerOneTurn == 1){
            $result['name'] = $player->playerOneName;
            $result['turnSymbol'] = "X";
        }else{
            $result['name'] = $player->playerTwoName;
            $result['turnSymbol'] = "O";
        }
        return $result;
    }

    //Obtener informacion de los datos del juego
    public function getBoardData(){
        //$gameId = $_SESSION['game'];
        $board = Board::find(1);
        return $board;
    }

    //Obrener informacion de los jugadores del juego
    public function getPlayersData(){
        //$gameId = $_SESSION['game'];
        $player = Players::find(1);
        return $player;
    }

    //Guardar turno del juego
    public function saveTurn(Request $request){
        $value = $request->input('move');
        $symbol = $request->input('symbol');
        //$board = Board::where('gameId','')->get();
        $board = Board::find(1);
        $board['pos'.$value] = $symbol;
        $moves = $this->setTurn($symbol);
        if($moves >= 0){
            if($moves > 8){
                return redirect()->route('board.finish');
            }
            $board->totalMoves = $moves;
            if($board->save()){
                if($this->validateMove($symbol, $board, $value, $moves)){
                    $this->updateWinner($symbol);
                    return redirect()->route('board.finish');
                }else{
                    return redirect()->route('board.index');
                }
            }else{
                echo "<script>alert('No se puede asignar turno')</script>";
            }
        }else{
            echo "<script>alert('No se puede guardar registro')</script>";
        }
    }

    //Modificar turno del jugador
    public function setTurn($symbol){
        $player = Players::find(1);
        if($symbol === 'X'){
            $p1turn = 0;
            $p2turn = 1;
            $player->playerOneMoves = $player->playerOneMoves + 1;
        }else{
            $p1turn = 1;
            $p2turn = 0;
            $player->playerTwoMoves = $player->playerTwoMoves + 1;
        }
        $ptotal = $player->playerOneMoves + $player->playerTwoMoves;
        $player->playerOneTurn = $p1turn;
        $player->playerTwoTurn = $p2turn;
        if($player->save()){
            $return = $ptotal;
        }else{
            $return = false;
        }
        return $return;
    }

    //Validar movimiento
    public function validateMove($symbol, $board, $pos, $tMoves){
        //Si la cantidad de movimientos es menor a 3 no valide
        if($tMoves < 3){
            return false;
        }

        $column = $pos % 3;
        if (!$column) {
            $column = 3;
        }

        //$row = ceil($pos / 3);
        $row = $pos;

        $arrayBoard = array(
            '0' => 0,
            '1' => $board->pos1,
            '2' => $board->pos2,
            '3' => $board->pos3,
            '4' => $board->pos4,
            '5' => $board->pos5,
            '6' => $board->pos6,
            '7' => $board->pos7,
            '8' => $board->pos8,
            '9' => $board->pos9
        );

        return $this->validateVertical($column, $symbol, $arrayBoard) || $this->validateHorizontal($row, $symbol, $arrayBoard) || $this->validateDiagonal($symbol, $arrayBoard);

    }

    //Validacion ganar horizontal
    public function validateHorizontal($row, $symbol, $board){
        /*return $board[$row] == $symbol &&
        $board[$row + 1] == $symbol &&
        $board[$row + 2] == $symbol;*/
        $return = false;
        if($row == 3 || $row == 6 || $row == 9){
            if($board[$row - 1] == $symbol && $board[$row - 2]){
                $return = true;
            }
        }elseif($row == 2 || $row == 5 || $row == 8){
            if($board[$row - 1] == $symbol && $board[$row + 1]){
                $return = true;
            }
        }elseif($row == 1 || $row == 4 || $row == 7){
            if($board[$row + 1] == $symbol && $board[$row + 2]){
                $return = true;
            }
        }
        return $return;
    }

    //Validacion ganar Vertical
    public function validateVertical($column, $symbol, $board){
        return $board[$column] == $symbol &&
        $board[$column + 3] == $symbol &&
        $board[$column + 6] == $symbol;
    }

    //Validacion ganar Diagonal
    public function validateDiagonal($symbol, $board){
        $win = $board[1] == $symbol &&
        $board[9] == $symbol;

        if (!$win) {
            $win = $board[3] == $symbol &&
            $board[7] == $symbol;
        }

        return $win && $board[5] == $symbol;
    }

    //Se encarga de actualizar el ganador en la base de datos de la partida
    public function updateWinner($winner = "X"){
        $player = $this->getPlayersData();
        if($winner === 'X'){
            $player->playerOneWin = 1;
            $player->playerTwoWin = 0;
            $player->playerOneTurn = 0;
            $player->playerTwoTurn = 1;
        }else{
            $player->playerOneWin = 0;
            $player->playerTwoWin = 1;
            $player->playerOneTurn = 1;
            $player->playerTwoTurn = 0;
        }
        $player->save();
    }

    //Se encarga de reiniciar el juego
    public function resetGame(){
        $player = $this->getPlayersData();
        $board = $this->getBoardData();
        $player->playerOneWin = 0;
        $player->playerTwoWin = 0;
        $player->playerOneMoves = 0;
        $player->playerTwoMoves = 0;
        $p1name = $player->playerOneName;
        $p2name = $player->playerTwoName;
        $player->playerOneName = $p2name;
        $player->playerTwoName = $p1name;
        $player->save();
        $board->delete();
        return redirect()->route('home');  
    }

}
