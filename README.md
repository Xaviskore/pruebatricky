## Cómo instalar y jugar tic tac toe

Para este proyecto es necesario tener apache, php, composer, "node js"  y mysql. Se trata del famoso
juego de papel llamado tricky.

1. Clonar el repositorio en un direcorio
2. Descomprimir en la misma carpeta el archivo "archivos-necesarios.rar"
3. Tener instalado composer o en dicho directorio
4. Crear la base de datos llamada "prueba" en servidor mysql
5. Ejecutar las migraciones con "php artisan migrate"
6. Ejecutar los seeders con "php artisan db:seed"
7. Abrir proyecto en navegador
8. Jugar.

Gracias
Javier Lopez

---