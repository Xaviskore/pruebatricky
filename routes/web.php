<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','home')->name('home');
Route::get('game','App\Http\Controllers\BoardController@index')->name('board.index');
Route::get('finish','App\Http\Controllers\BoardController@finish')->name('board.finish');
Route::get('reset','App\Http\Controllers\BoardController@resetGame')->name('board.reset');
//Route::view('new','new')->name('new');
Route::get('new','App\Http\Controllers\BoardController@new')->name('board.new');
Route::post('board/savePlayers','App\Http\Controllers\BoardController@savePlayers');
Route::post('board/saveTurn','App\Http\Controllers\BoardController@saveTurn');
Route::post('board/joinPlay','App\Http\Controllers\BoardController@joinPlay');
Route::view('join','join')->name('join');
//Route::get('/{post:playerOneName}','App\Http\Controllers\BoardController@showBoard')->name('board.showBoard');
