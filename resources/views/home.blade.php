@extends('layout')

@section('content')
    <div class="d-flex justify-content-center align-items-center">
        <div class="presentation">
            <h1>Bienvenido(s)</h1>
            <h2>Este es el juego de TRICKY o TIC TAC TOE</h2>
            <a class="btn btn-primary" href="{{ route('board.new') }}">Nueva Partida</a>
            <a class="btn btn-primary" href="{{ route('join') }}">Unirse a Partida</a> 
        </div>
    </div>
@endsection