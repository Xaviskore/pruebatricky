@extends('layout')

@section('content')
    <div class="d-flex justify-content-center align-items-center">
        <div class="presentation">
            <h1>
                El ganador es:
                @if($players->playerOneWin == 1)
                    {{ $players->playerOneName }}
                @elseif($players->playerTwoWin == 1)
                    {{ $players->playerTwoName }}
                @else
                    nadie
                @endif
            </h1>
            <a href="{{ route('board.reset') }}" class="btn btn-warning">Reiniciar</a>
        </div>
    </div>
@endsection