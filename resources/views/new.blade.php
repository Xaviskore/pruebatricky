@extends('layout')

@section('content')
    <div class="d-flex justify-content-center align-items-center">
        <form method="post" action="board/savePlayers">
            @csrf 
            <div class="presentation">
                <h3>Por favor ingresa nombre de jugadores</h3>

                <div class="player-name">
                    <label for="player-x">Jugador 1 (X)</label>
                    <input class="form-control" type="text" id="player-x" name="player-x" value="{{ $players->playerOneName }}" required />
                </div>

                <div class="player-name">
                    <label for="player-o">Jugador 2 (O)</label>
                    <input class="form-control" type="text" id="player-o" name="player-o" value="{{ $players->playerTwoName }}" required readonly/>
                </div><br>

                <button class="btn btn-primary" type="submit">Empezar</button>
            </div>
        </form>
    </div>
@endsection