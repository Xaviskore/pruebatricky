@extends('layout')

@section('content')
    <div class="d-flex justify-content-center align-items-center">
        <div class="presentation">
            <form method="post" action="board/saveTurn">
                @csrf
                <h2>Es el turno de {{ $result['name'] }} - {{ $result['turnSymbol'] }}</h2>
                <table id="tricky-table" class="table table-bordered border-primary">
                    <tr>
                        <?php $j = 0;?>
                        @for($i = 1; $i <= 9; $i++)
                            @if($j == 3)
                            <tr>
                            @endif                                   
                            <?php if($board['pos'.$i] == 0): ?>
                            <td><input type="radio" name="move" value="<?php echo $i;?>" required></td>
                            <?php else: ?>
                            <td><?php echo $board['pos'.$i]; ?></td>
                            <?php endif; ?>
                            <?php $j++; ?>
                            @if($j == 3)
                            </tr>
                            <?php $j = 0; ?>
                            @endif
                        @endfor
                    </tr>
                </table>
                <input type="hidden" value="{{ $result['turnSymbol'] }}" name="symbol">
                <button class="btn btn-danger" type="submit">Enviar</button>
            </form>
        </div>
    </div>
@endsection