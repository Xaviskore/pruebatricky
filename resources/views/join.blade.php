@extends('layout')

@section('content')
    <div class="d-flex justify-content-center align-items-center">
        <form method="post" action="board/joinPlay">
            @csrf
            <div class="presentation">
                <h3>Por favor ingresa ID de la partida</h3>

                <div class="player-name">
                    <label for="player-o">ID</label>
                    <input class="form-control" type="text" id="id" name="id" required />
                </div><br>

                <button class="btn btn-primary" type="submit">Empezar</button>
            </div>
        </form>
    </div>
@endsection