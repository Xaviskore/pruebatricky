<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boards', function (Blueprint $table) {
            $table->id();
            $table->string('gameId',100);
            $table->string('pos1', 10);
            $table->string('pos2', 10);
            $table->string('pos3', 10);
            $table->string('pos4', 10);
            $table->string('pos5', 10);
            $table->string('pos6', 10);
            $table->string('pos7', 10);
            $table->string('pos8', 10);
            $table->string('pos9', 10);
            $table->tinyinteger('totalMoves');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boards');
    }
};
