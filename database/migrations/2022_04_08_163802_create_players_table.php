<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->id();
            $table->string('game_Id',100);
            $table->string('playerOneName', 100);
            $table->string('playerTwoName', 100);
            $table->tinyinteger('playerOneTurn');
            $table->tinyinteger('playerTwoTurn');
            $table->tinyinteger('playerOneWin');
            $table->tinyinteger('playerTwoWin');
            $table->tinyinteger('playerOneMoves');
            $table->tinyinteger('playerTwoMoves');
            //$table->foreign('game_Id')->references('id')->on('board');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
};
