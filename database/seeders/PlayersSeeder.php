<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\models\Players;

class PlayersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $players = new Players;
        $players->game_Id = "0";
        $players->playerOneName = "Jugador 1";
        $players->playerTwoName = "Jugador 2";
        $players->playerOneTurn = 1;
        $players->playerTwoTurn = 0;
        $players->playerOneWin = 0;
        $players->playerTwoWin = 0;
        $players->playerOneMoves = 0;
        $players->playerTwoMoves = 0;

        $players->save();
    }
}
